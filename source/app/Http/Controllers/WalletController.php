<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    //

    public function deposit($user_id, Request $request)
    {
        $data = User::find($user_id);

        return view('wallets.deposit', compact('data'));

    }
    public function deposit_store($user_id, Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            'remark' => 'required',
        ]);
        $user = User::find($user_id);
        $user->deposit($request->input('amount'), ['description' => $request->input('amount')]);

        return redirect()->route('wallets.deposit', $user_id)
            ->with('success', 'Wallet deposit successfully');

    }

    public function withdraw($user_id, Request $request)
    {
        $data = User::find($user_id);

        return view('wallets.withdraw', compact('data'));

    }
    public function withdraw_store($user_id, Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            'remark' => 'required',
        ]);
        $user = User::find($user_id);
        $user->withdraw($request->input('amount'), ['description' => $request->input('amount')]);

        return redirect()->route('wallets.withdraw', $user_id)
            ->with('success', 'Wallet withdraw successfully');

    }

}