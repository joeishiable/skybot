@extends('layouts.blank')

@section('title', 'Page Title')

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="/css/elements/avatar.css">

<style>
    body {
        background-color: #f3f3f4;
    }

</style>

@stop
@section('content')

<div class="container-fluid">


    {!! Form::open(array('route' => ['wallets.deposit_store',$data->id],'method'=>'POST')) !!}

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">


            <div class="widget-content widget-content-area mb-3">
                <div class="text-center">
                    <h6 class="">ฝากเครดิต</h6>

                </div>
                <div class="text-center user-info">
                    <div class="avatar avatar-lg">
                        <img alt="avatar" src="/img/90x90.jpg" class="rounded-circle" />
                    </div>
                    <p class="">{{ $data->name }}</p>
                </div>

            </div>

            {{-- <legend>ฝากเครดิต</legend> --}}
            <div class="form-group">
                <label for="">จำนวนเงิน</label>
                {!! Form::text('amount', null, array('placeholder' => 'eg. 1000','class' =>
                'form-control','required'=>'required')) !!}
            </div>

            <div class="form-group">
                <label for="">ข้อความหมายเหตุ</label>
                {!! Form::textarea('remark', null, array('placeholder' => 'eg. ฝากเงิน','class' =>
                'form-control','rows'=>4,'required'=>'required'))
                !!}
            </div>

            {!! Form::submit('ฝาก',array('class'=>'btn btn-success btn-block ')) !!}
            {!! Form::button('ยกเลิก',array('class'=>'btn btn-danger btn-block close_modal')) !!}

            {{-- <a class="btn btn-primary close_modal" href="#">Dismiss</a> --}}
        </div>

    </div>

    {!! Form::close() !!}
</div>



@endsection


@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {



        $.validator.setDefaults({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                // element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
        $("form").validate();
        $('.close_modal').on('click', function () {
            parent.jQuery.fancybox.getInstance().close();
        });
    });

</script>
@stop

@if(session()->has('success'))
<script>
    parent.jQuery.fancybox.getInstance().close();
    parent.location.reload()

</script>

@endif
