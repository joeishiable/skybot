@extends('adminlte::page')

@section('title', 'Users Management')

@section('content_header')
{{-- <h1>Users Management</h1> --}}
<div class="row mb-2">
    <div class="col-sm-12">
        <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Buttons</li>
        </ol>
    </div>
</div>

@stop
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="statbox widget box box-shadow">
            <div class="widget-heading pt-3 pl-3 pr-3 pb-0">
                <h5>รายการสมาชิก</h5>
                <ul class="tools ">
                    <li><a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"><i
                                class="fa fa-plus fa-fw"></i>
                            เพิ่มสมาชิกใหม่</a></li>
                </ul>
            </div>
            <div class="table-responsive">
                <div class="widget-content widget-content-area">

                    <table class="table table-bordered mb-4">
                        <thead>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>ยูสเซอร์เนม</th>
                                <th>เครดิต</th>
                                <th>สิทธิ์</th>
                                <th>วันที่สมัคร</th>
                                <th width="280px">กระทำ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ number_format($user->balance) }}</td>
                                <td>
                                    @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                    <label class="badge badge-success">{{ $v }}</label>
                                    @endforeach
                                    @endif
                                </td>
                                <td>{{ $user->created_at->locale('th_TH')->format('d/m/Y h:i:s') }}</td>
                                <td>

                                    <div class="dropdown open">
                                        <button class="btn btn-xs btn-secondary dropdown-toggle" type="button"
                                            id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="triggerId">
                                            <a data-fancybox="" data-type="iframe"
                                                href="{{ route('wallets.deposit',$user->id) }}"
                                                class="font-bold dropdown-item"><i class="far fa-credit-card "></i> ฝาก
                                            </a>
                                            <a data-fancybox="" data-type="iframe"
                                                href="{{ route('wallets.withdraw',$user->id) }}"
                                                class="font-bold dropdown-item"><i class="far fa-credit-card "></i> ถอน
                                            </a>

                                            <a href="{{ route('users.edit',$user->id) }}" class="font-bold
                                dropdown-item"><i class="far fa-address-card fa-fw"></i> แก้ไข
                                            </a>
                                            <a href="/admin/member/1842/credit" class="font-bold dropdown-item"><i
                                                    class="far fa-credit-card fa-fw"></i>
                                                รายงานเครดิต
                                            </a>

                                            <a target="_blank" href="{{ route('users.show',$user->id) }}"
                                                class="font-bold dropdown-item"><i class="fa fa-info fa-fw"></i>
                                                ข้อมูล
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy',
                                            $user->id],'style'=>'display:inline']) !!}

                                            <button class="dropdown-item"><i class="fa fa-ban text-danger fa-fw"></i>
                                                ระงับ</button>

                                            {!! Form::close() !!}
                                        </div>
                                    </div>


                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<style>
    .fancybox-slide--iframe .fancybox-content {
        width: 500px;
        height: 550px;
        max-width: 90%;
        max-height: 90%;
        margin: 0;
    }

</style>
@stop
@section('plugins.Fancybox', true)
@section('js')
<script>
    $(document).ready(function () {
        $('.table-responsive').on('show.bs.dropdown', function () {
            $('.table-responsive').css("overflow", "inherit");
        });

        $('.table-responsive').on('hide.bs.dropdown', function () {
            $('.table-responsive').css("overflow", "auto");
        })
        $('[data-fancybox]').fancybox({
            // toolbar: true,
            smallBtn: true,
            iframe: {
                preload: false
            }
        })
    });

</script>
@stop
