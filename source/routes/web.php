<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', function() {
//     return view('home');
// })->name('home')->middleware('auth');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    // Route::resource('products','ProductController');

    Route::post('/wallets/deposit/{user_id}', 'WalletController@deposit_store')->name('wallets.deposit_store');
    Route::get('/wallets/deposit/{user_id}', 'WalletController@deposit')->name('wallets.deposit');

    Route::post('/wallets/withdraw/{user_id}', 'WalletController@withdraw_store')->name('wallets.withdraw_store');
    Route::get('/wallets/withdraw/{user_id}', 'WalletController@withdraw')->name('wallets.withdraw');

});